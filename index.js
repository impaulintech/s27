const http = require('http');
let port = 4000;

let directory = 
[
	{"name" : "Brandon", "email" : "brandon@gmai.com"},
	{"name" : "Jobert", "email" : "jobert@gmai.com"},
]

const server = http.createServer((req, res) => {

	//Get all users
	if (req.url === '/users' && req.method == 'GET') {

		res.writeHeader(200, {'Content-type':'text/plain'})
		res.write(JSON.stringify(directory));
		res.end();
		console.log(`User Accessing http://localhost:${port}/users`)

	}  
	//Add new user
	if (req.url === '/users' && req.method == 'POST') { 

		let requestBody = "";

		req.on('data', (data)=>{
			requestBody += data;
		})
		req.on('end', ()=>{
			console.log(typeof requestBody);
			requestBody = JSON.parse(requestBody);

			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}

			directory.push(newUser)
			console.log(directory)
			console.log(`User Accessing http://localhost:${port}/users`)

			res.writeHeader(200, {'Content-type':'application/json'})
			res.write(JSON.stringify(newUser));
			res.end();
		}) 
	}  

	//No content error 
	if (req.url !== '/users') {
		res.writeHeader(404, {'Content-type':'text/plain'}) 
		res.end('No content available here.');
		console.log(`User Accessing http://localhost:${port}${req.url}`)
	}

}).listen(port, 'localhost', console.log(`Listening to port ${port}...`));